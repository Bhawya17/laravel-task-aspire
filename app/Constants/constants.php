<?php

/*
|--------------------------------------------------------------------------
| Application Constants
|--------------------------------------------------------------------------
|
| 
|
*/

if(!defined('NO')) define('NO', 0);
if(!defined('YES')) define('YES', 1);

if(!defined('PAID')) define('PAID',1);
if(!defined('UNPAID')) define('UNPAID', 0);

if(!defined('LOAN_INITIATED')) define('LOAN_INITIATED',0);
if(!defined('LOAN_APPROVED')) define('LOAN_APPROVED',1);
if(!defined('LOAN_REJECTED')) define('LOAN_REJECTED',2);
if(!defined('LOAN_COMPLETED')) define('LOAN_COMPLETED',3);

if(!defined('LOAN_PAID')) define('LOAN_PAID',1);
if(!defined('LOAN_NOT_PAID')) define('LOAN_NOT_PAID',0);


if(!defined('LOAN_TERM_WEEK')) define('LOAN_TERM_WEEK','week');
if(!defined('LOAN_TERM_MONTH')) define('LOAN_TERM_MONTH','month');
if(!defined('LOAN_TERM_YEAR')) define('LOAN_TERM_YEAR','year');

if(!defined('INTEREST_PERCENTAGE')) define('INTEREST_PERCENTAGE', 1.75);
