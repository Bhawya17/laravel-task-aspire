<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name'] ?: rand(1,10000).rand(1,10000)));

            $model->attributes['token'] = generate_token();

            $model->attributes['token_expiry'] = generate_token_expiry();

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name'] ?: rand(1,10000).rand(1,10000)));
        
        });

        static::updating(function($model) {

        });

        static::deleting(function ($model){
            
        });

    }
}
