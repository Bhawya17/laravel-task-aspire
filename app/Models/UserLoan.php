<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLoan extends Model
{
    use HasFactory;

    protected $hidden = ['weekly_pay_amount'];

    protected $appends = ['user_loan_id', 'user_loan_unique_id','weekly_pay_formatted','status_formatted','approval_status'];


    public function getUserLoanIdAttribute() {

        return $this->id;
    }

    public function getUserLoanUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getWeeklyPayFormattedAttribute() {

        return number_format((float)$this->weekly_pay_amount, 2, '.', '');
    }

    public function getStatusFormattedAttribute() {

        $status = $this->status == LOAN_PAID ? 'Paid' : 'Not Paid';

        return $status;
    }

    public function getApprovalStatusAttribute() {

        $status = loan_status($this->is_approved);

        return $status;
    }

    /**
     * Scope a query to only approved loans.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query) {

        $query->where('user_loans.is_approved', LOAN_APPROVED);

        return $query;

    }

    /**
     * Scope a query to only approved loans.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaid($query) {

        $query->where('user_loans.status', LOAN_PAID);

        return $query;

    }

    /**
     * Scope a query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {
        return $query;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "UL"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "UL"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });

    }
}
