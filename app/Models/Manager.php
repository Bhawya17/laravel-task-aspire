<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    use HasFactory;

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name'] ?: rand(1,10000).rand(1,10000)));

            $model->attributes['token'] = generate_token();

            $model->attributes['token_expiry'] = generate_token_expiry();

        });

        static::created(function($model) {

            $model->attributes['unique_id'] = routefreestring(strtolower($model->attributes['name'] ?: rand(1,10000).rand(1,10000)));
        
        });

        static::updating(function($model) {

        });

        static::deleting(function ($model){
            
        });

    }
}
