<?php

/**
 * @method api_success()
 *
 * @uses api success message
 * 
 * @created Bhawya N 
 *
 * @updated Bhawya N
 *
 * @param Form data
 *
 * @return validation exception
 */
function api_success($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-success.'.$key, 0, Array('other_key' => $other_key), $locale);

}

/**
 * @method api_error()
 *
 * @uses api error message
 * 
 * @created Bhawya N 
 *
 * @updated Bhawya N
 *
 * @param Form data
 *
 * @return validation exception
 */
function api_error($key , $other_key = "" , $lang_path = "messages.") {

    if (!\Session::has('locale')) {

        $locale = \Session::put('locale', config('app.locale'));

    } else {

        $locale = \Session::get('locale');

    }
    return \Lang::choice('api-error.'.$key, 0, Array('other_key' => $other_key), $locale);

}

/**
 * @method custom_validator()
 *
 * @uses validation function
 * 
 * @created Bhawya N 
 *
 * @updated Bhawya N
 *
 * @param Form data
 *
 * @return validation exception
 */
function custom_validator($request, $request_inputs, $custom_errors = []) {

    $validator = Validator::make($request, $request_inputs, $custom_errors);

    if($validator->fails()) {

        $error = implode(',', $validator->messages()->all());

        throw new Exception($error, 101);
           
    }
}

/**
 * @method routefreestring()
 *
 * @uses used to remove the route parameters from the string
 * 
 * @created Bhawya N 
 *
 * @updated Bhawya N
 *
 * @param Form data
 *
 * @return validation exception
 */
function routefreestring($string) {

    $string = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $string));
    
    $search = [' ', '&', '%', "?",'=','{','}','$'];

    $replace = ['-', '-', '-' , '-', '-', '-' , '-','-'];

    $string = str_replace($search, $replace, $string);

    return $string;
    
}

/**
 * @method generate_token()
 *
 * @uses generate user unique token
 * 
 * @created Bhawya N 
 *
 * @updated Bhawya N
 *
 * @param Form data
 *
 * @return validation exception
 */
function generate_token() {
        
    return clean(Hash::make(rand() . time() . rand()));

}

/**
 * @method generate_token_expiry()
 *
 * @uses token expiry based on hours
 * 
 * @created Bhawya N 
 *
 * @updated Bhawya N
 *
 * @param Form data
 *
 * @return validation exception
 */
function generate_token_expiry() {

    $token_expiry_hour = 1;
    
    return time() + $token_expiry_hour*3600;  // 1 Hour
}


/**
 * @method clean()
 *
 * @uses used to remove the route parameters from the string
 * 
 * @created Bhawya N 
 *
 * @updated Bhawya N
 *
 * @param Form data
 *
 * @return validation exception
 */
function clean($string) {

    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}


function calculate_pay_amount($weeks,$amount){
   
   $interest = INTEREST_PERCENTAGE / 100; 

   $pay_amount = $interest / 12 * pow(1 + $interest / 12,$weeks) / (pow(1 + $interest / 12,$weeks) - 1) * $amount;

   return $pay_amount;

}

function loan_status($status) {

    $status_text = [
        LOAN_INITIATED => 'Loan Initiated',
        LOAN_APPROVED => 'Loan Approved',
        LOAN_REJECTED => 'Loan Rejected', 
        LOAN_COMPLETED => 'Loan Completed',
    ];

    return isset($status_text[$status]) ? $status_text[$status] : tr('WALLET_PAYMENT_TYPE_ADD_TEXT');

}

?>