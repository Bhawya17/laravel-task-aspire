<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use DB, Log, Hash, Validator, Exception, Setting;

use App\Models\Manager,App\Models\UserLoan;

class ManagerApiController extends Controller
{

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: 12;
    }

    /**
     * @method login()
     *
     * @uses Registered manager can login using their email & password
     * 
     * @created Bhawya N 
     *
     * @updated Bhawya N
     *
     * @param object $request - User mobile & Password
     *
     * @return Json response with manager details
     */
    public function login(Request $request) {

        try {
            
            DB::beginTransaction();

            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];

            custom_validator($request->all(), $rules);

            $manager = Manager::firstWhere('email', '=', $request->email);
            
            if(!$manager) {

                throw new Exception(api_error(1002), 1002);

            }

            if(Hash::check($request->password, $manager->password)) {

                $data = Manager::find($manager->id);
                
                DB::commit();
                
                return $this->sendResponse(api_success(1002), 1002, $data);

            } else {

                throw new Exception(api_error(1003), 1003);

            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method logout()
     *
     * @uses Logout the user
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param 
     * 
     * @return
     */
    public function logout(Request $request) {

        return $this->sendResponse(api_success(1001), 1001);

    }

    /**
     * @method user_loans()
     *
     * @uses List User Loans
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param 
     * 
     * @return
     */
    public function user_loans(Request $request) {

        try {

            $base_query = $total_query = UserLoan::where('is_approved',LOAN_INITIATED);

            $user_loans = $base_query->skip($this->skip)->take($this->take)->orderBy('created_at', 'desc')->get();
            
            $data['user_loans'] = $user_loans ?? [];

            $data['total'] = $total_query->count() ?? 0;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @method user_loan_status()
     *
     * @uses Update User Loan Status Approve/decline
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param 
     * 
     * @return
     */
    public function user_loan_status(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['user_loan_id' => 'required|exists:user_loans,id'];

            custom_validator($request->all(), $rules, $custom_errors = []);


            $user_loans = UserLoan::firstWhere('user_loans.id',  $request->user_loan_id);

            if(!$user_loans) {

                throw new Exception(api_error(1004), 1004);
                
            }

            $user_loans->is_approved = $user_loans->is_approved == LOAN_APPROVED ? LOAN_REJECTED : LOAN_APPROVED;

            if($user_loans->save()) {
                
                DB::commit();

                $status_code = $user_loans->is_approved == LOAN_APPROVED ? 1003 : 1004;

                return $this->sendResponse(api_success($status_code), $status_code, $user_loans);

            } 

            throw new Exception(api_error(1005),1005);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }


}
