<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use DB, Log, Hash, Validator, Exception, Setting;

use App\Models\User;

class UserAccountApiController extends Controller
{

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));

    }

    /**
     * @method register()
     *
     * @uses Registered user can register through manual login
     * 
     * @created Bhawya N 
     *
     * @updated Bhawya N
     *
     * @param Form data
     *
     * @return Json response with user details
     */
    public function register(Request $request) {
        try {

            DB::beginTransaction();

            $rules = [
                'name' => 'required|max:255|min:2',
                'address' => 'required',
                'mobile' => 'required|digits_between:6,13',
            ];

            custom_validator($request->all(), $rules);

            $user_details = User::firstWhere('mobile','=',$request->mobile);
           
            if($user_details) {

                throw new Exception(api_error(1000), 1000);

            }

            $user = new User;

            $user->name = $request->name ?? "";

            $user->email = $request->email ?? "";

            $user->mobile = $request->mobile ?? "";

            $user->address = $request->address ?? "";

            if($request->has('password')) {

                $user->password = Hash::make($request->password ?: "123456");

            }

            if($user->save()) {

                $data = User::find($user->id);

                DB::commit();

                return $this->sendResponse(api_success(1000), 1000, $data);

            } else {

                throw new Exception(api_error(1001), 1001);

            }

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }
   
    }

    /**
     * @method login()
     *
     * @uses Registered user can login using their email & password
     * 
     * @created Bhawya N 
     *
     * @updated Bhawya N
     *
     * @param object $request - User mobile & Password
     *
     * @return Json response with user details
     */
    public function login(Request $request) {

        try {
            
            $rules = [
                'mobile' => 'required',
                'password' => 'required',
            ];

            custom_validator($request->all(), $rules);

            $user = User::firstWhere('mobile', '=', $request->mobile);
            
            if(!$user) {

                throw new Exception(api_error(1002), 1002);

            }

            if(Hash::check($request->password, $user->password)) {

                $data = User::find($user->id);
                                
                return $this->sendResponse(api_success(1002), 1002, $data);

            } else {

                throw new Exception(api_error(1003), 1003);

            }

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }
    
    }

    /**
     * @method logout()
     *
     * @uses Logout the user
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param 
     * 
     * @return
     */
    public function logout(Request $request) {

        return $this->sendResponse(api_success(1001), 1001);

    }


}
