<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Helpers\Helper;

use DB, Log, Hash, Validator, Exception, Setting;

use App\Models\User,App\Models\UserLoan, App\Models\UserLoanPayment;

class UserLoanApiController extends Controller
{

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: 12;
    }

    /**
     * @method price_calculation()
     *
     * @uses Calculate Price based on user's amount and term
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param 
     * 
     * @return
     */
    public function loan_price_calculation(Request $request) {

        try {

            $rules = [
                'amount' => 'required',
                'term'=>'required'
            ];

            custom_validator($request->all(),$rules,[]);

            $weeks = 12;

            $amount = $request->amount;

            $data['total_amount'] = $amount;

            $data['term'] = $request->term;

            $data['interest'] = INTEREST_PERCENTAGE.'%';

            $data['total_weeks'] = $weeks;

            $pay_amount = calculate_pay_amount($weeks,$amount);   

            $data['pay_amount'] = number_format((float)$pay_amount, 2, '.', '');

            return $this->sendResponse($message = '' , $code = '', $data);
            
        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method apply_loan()
     *
     * @uses Apply Loan
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param 
     * 
     * @return
     */
    public function apply_loan(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                'amount' => 'required',
                'term'=>'required'
            ];

            custom_validator($request->all(),$rules,[]);

            $user_loans = UserLoan::Approved()->firstWhere('user_loans.user_id', $request->id);

            if($user_loans) {

                throw new Exception(api_error(1006), 1006);
                
            }

            $interest = INTEREST_PERCENTAGE / 100; 

            $weeks = 12;

            $amount = $request->amount;

            $pay_amount = calculate_pay_amount($weeks,$amount);   

            $user_loans = new UserLoan();

            $user_loans->user_id = $request->id;

            $user_loans->capital = $request->amount;

            $user_loans->weekly_pay_amount = number_format((float)$pay_amount, 2, '.', '');;

            $user_loans->interest_percentage = INTEREST_PERCENTAGE;

            $user_loans->installment = $weeks;

            $user_loans->total_pay = $user_loans->weekly_pay_amount*$weeks;

            $user_loans->plan_type = LOAN_TERM_WEEK;

            $user_loans->save();

            DB::commit();

            return $this->sendResponse(api_success(1005), 1005, $user_loans);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method loan_repay()
     *
     * @uses Loan Repayment
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param 
     * 
     * @return
     */
    public function loan_repay(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = [
                'user_loan_id' => 'required|exists:user_loans,id',
                'amount' => 'required',
            ];

            custom_validator($request->all(), $rules, $custom_errors = []);
            
            $user_loans = UserLoan::Paid()->firstWhere('user_loans.id', $request->user_loan_id);

            if($user_loans) {

                throw new Exception(api_error(1008), 1008);
                
            }

            $user_loans = UserLoan::Approved()
                ->where('user_loans.user_id', $request->id)
                ->where('user_loans.id', $request->user_loan_id)
                ->first();

            if(!$user_loans) {

                throw new Exception(api_error(1009), 1009);
                
            }

            if($request->amount < round($user_loans->weekly_pay_amount && $request->amount != $user_loans->weekly_pay_amount)) {

                throw new Exception(api_error(1007), 1007);
                
            }

            $user_loan_payments = new UserLoanPayment;

            $user_loan_payments->user_loan_id = $request->user_loan_id;

            $user_loan_payments->user_id = $request->id;

            $user_loan_payments->paid_amount = $request->amount;

            $user_loan_payments->paid_date = date('Y-m-d H:i:s');

            $user_loan_payments->status = PAID;

            $user_loan_payments->save();

            $user_loans->total_dues_paid += 1;

            $user_loans->status = $user_loans->installment == $user_loans->total_dues_paid ? LOAN_PAID : LOAN_NOT_PAID;

            $user_loans->save();

            DB::commit();

            return $this->sendResponse(api_success(1006), 1006, $user_loans);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());

        }

    }

    /**
     * @method user_loans()
     *
     * @uses List User Loans
     *
     * @created Bhawya N
     *
     * @updated Bhawya N
     *
     * @param 
     * 
     * @return
     */
    public function loans(Request $request) {

        try {

            $base_query = $total_query = UserLoan::where('user_id',$request->id)->CommonResponse();

            $user_loans = $base_query->skip($this->skip)->take($this->take)->orderBy('created_at', 'desc')->get();
            
            $data['user_loans'] = $user_loans ?? [];

            $data['total'] = $total_query->count() ?? 0;

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        }

    }

}
