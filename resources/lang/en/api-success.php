<?php

return [

    // Authentication
    1000 => 'User registered successfully',

    1001 => 'Logged out',

    1002 => 'successfully loggedin!!',

    1003 => 'Loan approved successfully!!',

    1004 => 'Loan declined successfully!!',

    1005 => 'Loan applied successfully. Waiting for Approval!!',

    1006 => 'Payment Success!!',
];
