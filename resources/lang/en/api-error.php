<?php

return [

    // Authentication
    1000 => 'User account already exist. Please login to continue!!',

    1001 => 'User Registration failed!!',

    1002 => 'You are not a registered user!!',

    1003 => 'Sorry, the mobile or password you entered do not match. Please try again',

    1004 => 'Loan details not exist!!',

    1005 => 'Loan status update failed!!',

    1006 => 'Already you have pending loan',

    1007 => 'Check your loan amount!!',

    1008 => 'Already you have paid the loan',

    1009 => 'Loan waiting for Approval!!',
];
