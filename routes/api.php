<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'user' ,'as' => 'user.', 'middleware' => 'cors'], function() {
	
	// user account handle
	Route::post('register','Api\UserAccountApiController@register');
    
    Route::post('login','Api\UserAccountApiController@login');

    // loan process
    Route::post('loan_price_calculation','Api\UserLoanApiController@loan_price_calculation');

    Route::post('apply_loan','Api\UserLoanApiController@apply_loan');

    Route::post('loan_repay','Api\UserLoanApiController@loan_repay');

    Route::post('loans','Api\UserLoanApiController@loans');

});

Route::group(['prefix' => 'manager' ,'as' => 'manager.', 'middleware' => 'cors'], function() {

	Route::post('login','Api\ManagerApiController@login');

    // Manager Approval

	Route::post('user_loans','Api\ManagerApiController@user_loans');

    Route::post('user_loan_status','Api\ManagerApiController@user_loan_status');

});