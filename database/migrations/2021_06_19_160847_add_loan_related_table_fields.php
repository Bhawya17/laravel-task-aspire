<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoanRelatedTableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_loans', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('user_id');
            $table->float('capital')->default(0.00);
            $table->float('interest_percentage')->default(0.00);
            $table->float('total_pay')->default(0.00);
            $table->float('weekly_pay_amount')->default(0.00);
            $table->integer('installment')->default(0);
            $table->integer('total_dues_paid')->default(0);
            $table->string('plan_type')->default(LOAN_TERM_WEEK);
            $table->tinyInteger('is_approved')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });

        Schema::create('user_loan_payments', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->default(rand());
            $table->integer('user_id');
            $table->integer('user_loan_id');
            $table->float('paid_amount')->default(0.00);
            $table->datetime('paid_date')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
